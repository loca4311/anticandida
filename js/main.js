
$('#open_mob_menu').click(function () {
    $(this).toggleClass('active')
    $('#menu-magazin-ru').toggle()
    $('#mob_menu').toggle()
})

// SCROLL MENU---------------
$('#open_mob_menu_scroll').click(function () {
    $(this).toggleClass('active')
    $('#menu-magazin-ru').toggle()
    $('#mob_menu').toggleClass('sticky__menu').toggle()
})


console.log('test')

// SLIDER-------------
const swiper = new Swiper(".reviews__slider",{
    autoHeight: true,
    spaceBetween: 100,
    centeredSlides: !0,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
    },
    pagination: {
        el: ".swiper-pagination"
    },
    mousewheel: !0,
    keyboard: !0
})

// MORE TEXT---------------
for (let i = 1; i <= 3; i++) {
    $(`.show_hide${i}`).click(function (){
        $(`#courses__item-content${i}`).toggleClass('active__more');
            if($(this).hasClass("moreLink"))
            {
                $(this).addClass("lessLink");
                $(`#coursesItem${i}`).addClass("courses__item-height");
                $(this).removeClass("moreLink");
            }
            else{
                $(this).addClass("moreLink");
                $(this).removeClass("lessLink");
                $(`#coursesItem${i}`).removeClass("courses__item-height");

            }
    });

    $(`.show_hide${i} span`).click(function(){
        $(this).text(function(i, v){
            return v === 'Подробнее' ? 'Скрыть' : 'Подробнее'
        });
    });
}

